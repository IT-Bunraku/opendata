from django.contrib import admin

from .models import *

##################################################################################################

class CKAN_HubAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'realm', 'version')
    list_filter   = ('version',)
    list_editable = ('realm', 'version')

admin.site.register(CKAN_DataHub, CKAN_HubAdmin)

#*******************************************************************************

class CKAN_SetAdmin(admin.ModelAdmin):
    list_display  = ('hub','alias', 'when')
    list_filter   = ('when','hub__version','hub__alias',)

admin.site.register(CKAN_DataSet, CKAN_SetAdmin)

#*******************************************************************************

class CKAN_ResAdmin(admin.ModelAdmin):
    list_display  = ('dataset','alias', 'type_pkg','type_res','type_url', 'when', 'id_pkg', 'id_res', 'id_rev')
    list_filter   = ('type_pkg','type_res','when','dataset__hub__version','dataset__hub__alias')
    #list_editable = ('realm', 'version')

admin.site.register(CKAN_Resource, CKAN_ResAdmin)

##################################################################################################

class SchemaAdmin(admin.ModelAdmin):
    list_display  = ('alias',)

admin.site.register(DataSchema, SchemaAdmin)

#*******************************************************************************

class PredicateAdmin(admin.ModelAdmin):
    list_display  = ('schema','alias')
    list_filter   = ('schema__alias',)

admin.site.register(DataPredicate, PredicateAdmin)

#*******************************************************************************

class SourceAdmin(admin.ModelAdmin):
    list_display  = ('schema','alias','adapter','link')
    list_filter   = ('schema__alias','adapter')
    list_editable = ('adapter','link')

admin.site.register(DataSource, SourceAdmin)

##################################################################################################

class GenericMetaPropInline(admin.TabularInline):
    model = GenericMetaProp
    # readonly_fields = ('slug','created')
    #fields = ('code','namespace')
    # related_search_fields = {'label' : ('name','slug')}
    extra=1 
    

class GenericMetaPropAdmin(admin.ModelAdmin):
    pass

class ObjectTypeAdmin(admin.ModelAdmin):
    pass

class AttributeMappingInline(admin.TabularInline):
    model = AttributeMapping
    # readonly_fields = ('slug','created')
    #fields = ('code','namespace')
    # related_search_fields = {'label' : ('name','slug')}
    extra=1 

class EmbeddedMappingInline(admin.TabularInline):
    model = EmbeddedMapping
    # readonly_fields = ('slug','created')
    #fields = ('code','namespace')
    # related_search_fields = {'label' : ('name','slug')}
    extra=1 
    
class ObjectMappingAdmin(admin.ModelAdmin):
    search_fields = ['content_type__name' ]
    inlines = [   AttributeMappingInline, EmbeddedMappingInline]
    pass
    
class AttributeMappingAdmin(admin.ModelAdmin):
    pass

        
class EmbeddedMappingAdmin(admin.ModelAdmin):
    pass
    
class NamespaceAdmin(admin.ModelAdmin):
    list_display = ('uri','prefix','notes')
    fields = ('uri','prefix','notes')
#    related_search_fields = {'concept' : ('pref_label','definition')}
    #list_editable = ('name','slug')
    search_fields = ['uri','prefix']    

    
admin.site.register(Namespace, NamespaceAdmin)  
admin.site.register(GenericMetaProp,GenericMetaPropAdmin)
admin.site.register(ObjectType, ObjectTypeAdmin)
admin.site.register(ObjectMapping, ObjectMappingAdmin)
admin.site.register(AttributeMapping, AttributeMappingAdmin)
admin.site.register(EmbeddedMapping, EmbeddedMappingAdmin)

