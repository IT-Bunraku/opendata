subdomains = {
    'geo':    dict(icon='globe',          title="Geo"),

    'data':   dict(icon='database',       title="Data"),
    'linked': dict(icon='link',           title="Linked"),
}

