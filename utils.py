from gestalt.web.utils       import *

from uchikoma.connector.utils       import *
from uchikoma.connector.models      import *
from uchikoma.connector.schemas     import *

################################################################################

from SPARQLWrapper import SPARQLWrapper as SparqlWrapper
from SPARQLWrapper import JSON          as SparqlJSON

#*******************************************************************************

PROTOCOLs = (
    ('postgres', "PostgreSQL database"),
    ('mysql',    "MySQL database"),

    ('mongodb',  "MongoDB server"),
    ('couchdb',  "CouchDB server"),

    ('gremlin',  "Gremlin"),
    ('neo4j',    "Neo4j Cypher"),

    ('hdfs',     "Hadoop F.S"),

    ('elastic',  "Elastic Search"),
)

FORMATs = (
    ('json',     "JSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),

    ('json-ld',  "JSON-LD"),
    ('rdf',      "RDF"),
    ('n3',       "N3 / Turtle"),

    ('geojson',  "GeoJSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),
)

CORTEXs = (
    ('wernick', "Wernick"),
    ('broca',   "Broca"),

    ('reason',  "Reasoner"),
    ('learn',   "Learner"),
    ('assess',  "Assessement"),
    ('witness', "Witnessing"),
)

