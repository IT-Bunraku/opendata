from django.conf.urls import include, url

from gestalt.web.utils import Reactor

from uchikoma.opendata.views.linked  import Homepage, CKAN, Djubby, RDF_IO, Neo4j

################################################################################

urlpatterns = Reactor.router.urlpatterns('linked',
    url(r'^sparql/([\w\d\-\_]+)/(.*)$',                       Djubby.Dispatcher.as_view(), name='djubby'),

    url(r'rdf/model/(?P<model>[^\/]+)/id/(?P<id>\d+)$',       RDF_IO.to_rdfbyid, name='to_rdfbyid'),
    url(r'rdf/model/(?P<model>[^\/]+)/key/(?P<key>.+)$',      RDF_IO.to_rdfbykey, name='to_rdfbykey'),
    url(r'rdf/publish/(?P<model>[^\/]+)/(?P<id>\d+)$',        RDF_IO.pub_rdf, name='pub_rdf'),
    url(r'rdf/sync/(?P<models>[^\/]+)$',                      RDF_IO.sync_remote, name='sync_remote'),

    url(r'^ckan/$',                                           CKAN.homepage, name='ckan_homepage'),
    url(r'^ckan/(?P<hub_id>.+)/$',                            CKAN.overview, name='ckan_overview'),
    url(r'^ckan/(?P<hub_id>.+)/(?P<ds_id>.+)/$',              CKAN.dataset, name='ckan_dataset'),
    url(r'^ckan/(?P<hub_id>.+)/(?P<ds_id>.+)/(?P<res_id>.+)', CKAN.resource, name='ckan_resource'),

    #url(r'^$',                                                Homepage.as_view(), name='homepage'),
)

