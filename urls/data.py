from django.conf.urls import include, url

from gestalt.web.utils import Reactor

#from uchikoma.opendata               import rest
from uchikoma.opendata.views         import data as Home

################################################################################

from uchikoma.opendata.patterns.documents.views import HomeView

urlpatterns = Reactor.router.urlpatterns('data',
    #url(r'^neo4j/(.+)/?$',  Neo4j.homepage, name='graffeine'),
    url(r'^mongo/',         include('mongonaut.urls')),
    url(r'^sql/',           include('uchikoma.console.urls.explorer')),

    #url(r'^$',              Home.Homepage.as_view(), name='homepage'),

    url(r'^patterns/', [
        url(r'^notifications/', include('notifications.urls')),
        url(r'^documents/', include('documents.urls')),
        url(r'^accounts/', include('profiles.urls')),
        url(r'^api/', include('api.urls')),
        url(r'^blog/', include("blog.urls")),
        url(r'^', include('documents.legacy_urls')),

        url(r'^$', HomeView.as_view(), name='home'),
    ]),
)

