#-*- coding: utf-8 -*-

from uchikoma.opendata.shortcuts import *

################################################################################

@Reactor.router.register_route('data', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "data/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

#@Reactor.router.register_route('data', r'^tokenize$', strategy='login')
#class NLP_Tokenizer(NLP_Tool):
#    form          = TokenizeForm
#    template_name = 'data/views/tokenizer.html'
#
#    def process(self, request, context, data):
#        context['results'] = self.nlp_parse('en', data['sentences'])
#
#        return context

################################################################################

#from . import custom

#from . import ontologies

#*******************************************************************************

#urlpatterns = fqdn.urlpatters

