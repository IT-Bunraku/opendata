# -*- coding: utf-8 -*-

from uchikoma.opendata.shortcuts import *

import os, logging
from sets import Set

import urllib, mimeparse
from urllib2 import URLError

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic import View
from django.template import Template, Context
from django.utils.datastructures import MultiValueDictKeyError

from rdflib import Namespace, URIRef, Literal
from rdflib.graph import ConjunctiveGraph

from SPARQLWrapper import SPARQLWrapper, JSON

from gestalt.web.helpers import Reactor

#*******************************************************************************

rdf    = Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#")
rdfs   = Namespace("http://www.w3.org/2000/01/rdf-schema#")
xsd    = Namespace("http://www.w3.org/2001/XMLSchema#")
owl    = Namespace("http://www.w3.org/2002/07/owl#")
config = Namespace("http://richard.cyganiak.de/2007/pubby/config.rdf#")
dc     = Namespace("http://purl.org/dc/elements/1.1/")
dct    = Namespace("http://purl.org/dc/terms/")
skos   = Namespace("http://www.w3.org/2004/02/skos/core#")
foaf   = Namespace("http://xmlns.com/foaf/0.1/")

################################################################################

class Configuration:
    """Configuration using the Borg design pattern"""

    __shared_state = { "data" : None, "path" : None, "graph" : None, "endpoint" : None}

    def __init__(self, path=None):
        self.__dict__ = self.__shared_state
        if (self.data == None):
            if (path == None):
                raise ValueError("djubby's configuration MUST be initialized a first time, read http://code.google.com/p/djubby/wiki/GettingStarted")
            else:
                self.path = os.path.abspath(path)
                logging.debug("Reading djubby's configuration from %s..." % self.path)
                if (not os.path.exists(self.path)):
                    raise ValueError("Not found a proper file at '%s' with a configuration for djubby. Please, provide a right path" % self.path)

                data = ConjunctiveGraph()
                data.bind("conf", config) 
                try:
                    data.load(path, format='n3') 
                except Exception, e:
                    raise ValueError("Not found a proper N3 file at '%s' with a configuration for djubby. Please, provide a valid N3 file" % self.path)

                self.data = data
                try:
                    self.graph = self.get_value("sparqlDefaultGraph")
                    self.endpoint = self.get_value("sparqlEndpoint")
                except Exception, e:
                    raise ValueError("Not found the graph not the endpoint that it's supposed djubby have to query. Please, provide a right donfiguration")

                logging.info("Using <%s> as default graph to query the endpoint <%s>" % (self.graph, self.endpoint))
                self.__class__.__dict__['_Configuration__shared_state']["data"] = data #FIXME

    def get_values(self, prop):
        return rdf_values(self.data, predicate=config[prop])

    def get_value(self, prop):
        return rdf_value(self.data, predicate=config[prop])

################################################################################

class URI:
    parent = property(lambda self: self._prnt)
    conf   = property(lambda self: self.parent.conf)

    def __init__(self, ancestor, uri):
        self._prnt = ancestor
        if (type(uri)==URIRef):
            self.uri = unicode(uri)
        else:
            self.uri = uri
        self.label = uri2curie(self.uri, self.conf.data.namespaces())
        self.url = self.uri2url(self.uri)

    def uri2url(self, uri):
        datasetBase = self.conf.get_value("datasetBase")
        webBase = self.conf.get_value("webBase")
        return uri.replace(datasetBase, webBase)

    def __str__(self):
        return self.uri

    def __cmp__(self, o):
        return cmp(self.uri, o.uri)

    def __eq__(self, o):
        return self.uri.__eq__(o.uri)

    def __hash__(self):
        return self.uri.__hash__()

def str2uri(uri):
    if (type(uri)==str or type(uri)==unicode):
        return URIRef(uri)
    else:
        return uri

def uri2str(uri):
    if (type(uri)==URIRef(uri)):
        uri = unicode(uri)
    return urllib.unquote(uri)


def uri2curie(uri, namespaces):
    url, fragment = splitUri(uri)
    for prefix, ns in namespaces: #improve the performace of this operation       
        if (unicode(ns) == url):
            return "%s:%s" % (prefix, fragment)
    return uri

def splitUri(uri):
    if ("#" in uri):
        splitted = uri.split("#")
        return ("%s#"%splitted[0], splitted[1])
    else:
        splitted = uri.split("/")
        return ("/".join(splitted[:-1])+"/", splitted[-1])

################################################################################

formats = {
    "data" : { "default":"application/rdf+xml", "xml":"application/rdf+xml", "n3":"text/n3" },
    "page" : { "default":"text/html", "html":"text/html", "xhtml":"application/xhtml+xml" }
}

class Http303(HttpResponseRedirect):
    status_code = 303

################################################################################

#FIXME: duplicate function because circular import
def str2uri(uri):
    if (type(uri)==str or type(uri)==unicode):
        return URIRef(uri)
    else:
        return uri

def rdf_values(graph, subject=None, predicate=None):
    return graph.objects(subject=str2uri(subject), predicate=predicate)

def rdf_value(graph, subject=None, predicate=None, lang=None):
    if (lang == None):
        try:
            return str(rdf_values(graph, subject, predicate).next())
        except StopIteration:
            return ""
    else:
        values = rdf_values(graph, subject, predicate)
        for value in values:
            if (value.language == lang):
                return value
        return ""

def rdf_predicates(graph, subject=None):
    return graph.predicate_objects(str2uri(subject))

################################################################################

class Resource:
    queries = {
                "ask"      : "ASK { GRAPH <%s> { <%s> ?p ?o } }",
                "describe" : "DESCRIBE <%s> FROM <%s>"
              }

    parent = property(lambda self: self._prnt)
    conf   = property(lambda self: self.parent.conf)

    def __init__(self, ancestor, uri):
        self._prnt = ancestor

        logging.debug("Trying to build resource with URI <%s>..." % uri)
        self.uri = uri2str(uri)
        self.graph = self.conf.graph
        self.endpoint = self.conf.endpoint
        if (self.__ask__()):
            logging.info("Successfully found the resource with URI <%s> on this dataset" % self.uri)
        else:
            #FIXME: patch to support incosistencies on the URIs
            cleanuri = self.uri
            self.uri = self.quote(self.uri)
            logging.debug("Not found on the first try, trying the encoded URI  <%s>..." % self.uri)
            if (self.__ask__()):
                logging.info("Successfully found the resource with URI <%s> on this dataset" % self.uri)
            else:
                raise ValueError("Resource with URI <%s> not found on this dataset" % self.uri)            

    def quote(self, uri):
        uri = uri2str(uri)
        #return urllib.quote(uri)
        fixUnescapedCharacters = self.conf.get_value("fixUnescapedCharacters")
        for c in fixUnescapedCharacters:
            uri = uri.replace(c, urllib.quote(c))
        return uri

    def get_triples(self):
        sparql = SPARQLWrapper(self.endpoint)
        sparql.setQuery(self.queries["describe"] % (self.uri, self.graph))
        g = sparql.query().convert()
        logging.debug("Returning %d triples describing resource <%s>" % (len(g), self.uri))
        #FIXME: enrich with metadata
        for prefix, namespace in self.conf.data.namespaces():
            g.bind(prefix, namespace) 
        return g

    def get_uri(self):
        return uri

    def get_data_url(self):
        return self.parent.get_document_url(self.uri, "data")

    def get_page_url(self):
        return self.parent.get_document_url(self.uri, "page")

    def get_data(self):
        return get_data_xml()

    def get_data_xml(self):
        g = self.get_triples()
        return g.serialize(format="pretty-xml")

    def get_data_n3(self):
        g = self.get_triples()
        return g.serialize(format="n3")

    def get_page(self):
        return get_page_html()

    def get_page_html(self):
        g = self.get_triples()
        tpl = Template(self.__read_template__())

        data = {}
        data["uri"] = URI(self.parent, self.uri)
        lang = self.conf.get_value("defaultLanguage")
        data["lang"] = lang
        label = rdf_value(g, self.uri, rdfs["label"], lang)
        if (len(label)>0):
            data["label"] = label
        else:
            data["label"] = self.uri
        datasetBase = self.conf.get_value("datasetBase")
        webBase = self.conf.get_value("webBase")
        data["data"]  = self.get_data_url()
        data["project"] = self.conf.get_value("projectName")
        data["homelink"] = self.conf.get_value("projectHomepage")
        data["endpoint"] = self.conf.get_value("sparqlEndpoint")
        depiction = rdf_value(g, self.uri, foaf["depiction"])
        if (len(depiction)>0):
            data["depiction"] = depiction

        data["rows"] = self.__get_rows__(g)

        ctx = Context(data)
        return tpl.render(ctx)

    def __ask__(self):
        sparql = SPARQLWrapper(self.endpoint)
        query = self.queries["ask"] % (self.graph, self.uri)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        if (results.has_key("boolean")):
            # expected answer according SPARQL Protocol
            if (results["boolean"]):
                return True
        elif (results.has_key("results") and results["results"].has_key("bindings") and len(results["results"]["bindings"])>0):
            # I don't know why, but virtuoso sometimes uses __ask_retval
            # http://docs.openlinksw.com/virtuoso/rdfsparql.html
            if (bool(results["results"]["bindings"][0]["__ask_retval"]["value"])):
                return True
        else:
            return False
        return False

    def __get_rows__(self, g):
        rows = {}
        for p, o in rdf_predicates(g, self.uri):
            prop = URI(self.parent,p)
            if (not rows.has_key(prop)):
                rows[prop] = []
            if (type(o) == URIRef):
                rows[prop].append(URI(self.parent,o))
            elif (type(o) == Literal):
                item = {}
                item["literal"] = unicode(o)
                if (o.language):
                    item["language"] = o.language
                if (o.datatype):
                    item["datatype"] = uri2curie(o.datatype, self.conf.data.namespaces())
                rows[prop].append(item)
            else:
                rows[prop].append(o)
        return rows

    def __read_template__(self, name="resource"):
        path = Reactor.FILES('templates', 'djubby', "%s.html" % name)
        logging.debug("Reading template '%s' from %s" % (name, path))
        f = open(path, "r")
        content = f.read()
        f.close()
        return content

################################################################################

class Dispatcher(View):
    def get(self, request, endpoint='dbpedia', ref=None):
        cfg_path = Reactor.SPECS('config', 'djubby', '%s.n3' % endpoint)

        if not os.path.exists(cfg_path):
            raise Http404("Undefined Djubby config for SparQL endpoint '%s' !" % endpoint)

        setattr(self, 'conf', Configuration(cfg_path))

        logging.debug("Dispatching request on '%s'..." % ref)

        if (ref == None or len(ref) == 0):
            index = self.conf.get_value("indexResource")

            index = index.replace(self.conf.get_value("datasetBase"), self.conf.get_value("webBase"))

            logging.debug("Redirecting to the index resource...")

            return HttpResponseRedirect(index)
        else:
            try:
                uri, prefix = self.url_handler(ref)

                resource = Resource(self, uri)
            except ValueError, ve:
                logging.error("Error processing request for '%s': %s" % (ref, str(ve)))
                raise Http404(ve)
            except URLError, ue:
                logging.error("Error retrieving data for '%s': %s" % (ref, str(ue)))
                raise Http404(ue)

            if (prefix == None):
                prefix = self.preferred_prefix(request)
                get_url = getattr(resource, "get_%s_url" % prefix)
                url = get_url()
                logging.debug("Redirecting to the %s representation of %s: %s" % (prefix, uri, url))
                return Http303(url)
            else:         
                output = self.preferred_output(request, prefix)
                func = getattr(resource, "get_%s_%s" % (prefix, output))
                mimetype = self.get_mimetype(prefix, output)            
                logging.debug("Returning the %s representation of %s serialized as %s" % (prefix, uri, output))       
                resp = func()
                return HttpResponse(resp, content_type=mimetype)

    #***************************************************************************

    def url_handler(self, ref):
        uri = None
        prefix = None
        datasetBase = self.conf.get_value("datasetBase")
        webBase = self.conf.get_value("webBase")
        webResourcePrefix = self.conf.get_value("webResourcePrefix")

        if (len(webResourcePrefix) == 0):
            splitted = ref.split("/")
            prefix = splitted[0]
            if (prefix in self.supported_prefixes()):
                uri = "%s%s" % (datasetBase, ref[len(prefix)+1:])
                return uri, prefix
            else:
                prefix = None
                uri = datasetBase + ref
                return uri, prefix        
        else:
            if (ref.startswith(webResourcePrefix)):
                prefix = None
                uri = datasetBase + ref
                return uri, prefix
            else:
                splitted = ref.split("/")
                prefix = splitted[0]
                if (prefix in self.supported_prefixes()):
                    uri = datasetBase + ref.replace(prefix+"/", self.conf.get_value("webResourcePrefix"))
                    return uri, prefix
                else:
                    raise ValueError("Unsupportet type '%s'" % splitted[0])

    #***************************************************************************

    def preferred_format(self, request):
        try:
            accept = request.META["HTTP_ACCEPT"]
        except KeyError:
            accept = formats["data"]["xml"]
        return mimeparse.best_match(self.supported_formats(), accept)

    def preferred_prefix(self, request):
        return self.get_prefix(self.preferred_format(request))

    def preferred_output(self, request, prefix):
        if (prefix == "page"):
            return "html"
        else:
            try:
                output = request.GET["output"]
                if (output in self.supported_outputs()):
                    return output
                else:
                    return "xml"
            except MultiValueDictKeyError:
                format = self.preferred_format(request)
                for output, mimetype in formats["data"].items():
                    if (output != "default"):
                        if (mimetype == format):
                            return output
                return "xml"

    #***************************************************************************

    def supported_prefixes(self):
        return formats.keys()

    def supported_formats(self):
        fs = Set()
        for f in formats.itervalues():
            for m in f.itervalues():
                fs.add(m)
        return list(fs)

    def supported_outputs(self):
        outputs = Set()
        for f in formats.itervalues():
            for o in f.iterkeys():
                if (not o == "default"):
                    outputs.add(o)
        return list(outputs)

    #***************************************************************************

    def get_mimetype(self, prefix, output):
        if (not prefix in formats):
            prefix = "data"
        if (not output in formats[prefix]):
            output = "default"
        return formats[prefix][output]

    def get_prefix(self, mimetype):
        for prefix, f in formats.items():
            mimes = f.values()
            if mimetype in mimes:
                return prefix
        return "data"

    def get_document_url(self, uri, prefix, conf=None):
        webResourcePrefix = self.conf.get_value("webResourcePrefix")
        datasetBase = self.conf.get_value("datasetBase")
        webBase = self.conf.get_value("webBase")
        if (len(webResourcePrefix) == 0):
            return "%s%s/%s" % (webBase, prefix, uri[len(datasetBase):])
        else:
            return uri.replace(datasetBase, webBase).replace(webResourcePrefix, "%s/" % prefix)

