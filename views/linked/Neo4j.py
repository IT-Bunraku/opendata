#-*- coding: utf-8 -*-

from uchikoma.semantics.shortcuts import *
from uchikoma.opendata.shortcuts import *

################################################################################

@render_to('linked/views/neo4j/home.html')
def homepage(request, alias='default'):
    return context(
        opendata={
            'ckan': {
                'datahub':   CKAN_DataHub.objects.all(),
                'dataset':   CKAN_DataSet.objects.all(),
                'resource':  CKAN_Resource.objects.all(),
            },
            'rdf': {
                'namespace': RdfNamespace.objects.all(),
                'ontology':  RdfOntology.objects.all(),
            },
            'sparql': {
                'endpoint':  SparqlEndpoint.objects.all(),
                'ontology':  SparqlOntology.objects.all(),
                'query':     SparqlQuery.objects.all(),
            },
        },
    )

