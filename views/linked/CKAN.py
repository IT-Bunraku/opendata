#-*- coding: utf-8 -*-

from uchikoma.semantics.shortcuts import *
from uchikoma.opendata.shortcuts import *

################################################################################

@render_to('linked/views/ckan/home.html')
def homepage(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context(
        opendata={
            'ckan': {
                'datahub':   CKAN_DataHub.objects.all(),
                'dataset':   CKAN_DataSet.objects.all(),
                'resource':  CKAN_Resource.objects.all(),
            },
            'rdf': {
                'namespace': RdfNamespace.objects.all(),
                'ontology':  RdfOntology.objects.all(),
            },
            'sparql': {
                'endpoint':  SparqlEndpoint.objects.all(),
                'ontology':  SparqlOntology.objects.all(),
                'query':     SparqlQuery.objects.all(),
            },
        },
    )

################################################################################

@render_to('linked/views/ckan/hub.html')
def overview(request, hub_id):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

################################################################################

@render_to('linked/views/ckan/dataset.html')
def dataset(request, hub_id, ds_id):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

################################################################################

@render_to('linked/views/ckan/resource.html')
def resource(request, hub_id, ds_id, res_id):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

