#-*- coding: utf-8 -*-

import quepy

################################################################################

class DatenExt(ReactorExt):
    def initialize(self):
        self._reg = {}
        self._std = {}
        self._map = {}

        self.declare('bool',  bool)
        self.declare('int',   int)
        self.declare('float', float)

        self.declare('str',   str)
        self.declare('text',  unicode)

        self.declare('email')
        self.declare('phone')

        self.declare('link')
        self.declare('uuid')

        self.declare('json',  json)
        self.declare('yaml',  yaml)

    def declare(self, alias, *classes):
        if alias not in self._std:
            self._reg[alias] = {
                '': '',
            }

            for hnd in classes:
                if hnd not in self._std:
                    self._std[hnd] = alias

    def register(self, verse, narrow, provider):
        if verse not in self._map:
            self._map[verse] = {}

        alias = narrow

        if narrow in self._std:
            alias = self._std[narrow]

        if alias in self._reg:
            self._map[verse][alias] = provider

        return provider

    def field(self, verse, narrow, *args, **kwargs):
        alias = None

        if narrow in self._reg:
            alias = narrow
        elif narrow in self._std:
            alias = self._std[narrow]

        if verse in self._map:
            if alias in self._map[verse]:
                prvd = self._map[verse][alias]

                return prvd(*args, **kwargs)

################################################################################

class BaseProvider(object):
    manager = property(lambda self: self._mgr)
    name    = property(lambda self: self._key)

    def __init__(self, mgr, name, *args, **kwargs):
        self._mgr = mgr
        self._key = name
        
        self._reg = {}
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    def ns(self, key, auto=False, default=None):
        if key in self._reg:
            return self._reg[key]
        elif auto:
            resp = quepy.install("neurochip.semantic.%s" % key)

            self._reg[key] = resp

            return resp
        else:
            return default
    
################################################################################

class BaseIntent(object):
    def __init__(self, prn, raw, *args, **kwargs):
        self._prn = prn
        self._raw = raw
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prn)
    message  = property(lambda self: self._raw)

    manager  = property(lambda self: self.provider.manager)
    text     = property(lambda self: self.message['text'])

    @property
    def channel(self):
        return self.manager.slack_client.server.channels.find(self.message['channel'])

    def ns(self, *args, **kwargs): return self.provider.ns(*args, **kwargs)

################################################################################

class BaseResults(object):
    def __init__(self, prnt, raw, *args, **kwargs):
        self._won = prnt
        self._raw = raw
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    wonder  = property(lambda self: self._won)
    raw     = property(lambda self: self._raw)

    channel = property(lambda self: self.wonder.channel)

    def __getitem__(self, key,default=None):
        return self.raw.get(key, default)

################################################################################

class BaseWidget(object):
    def __init__(self, template_path, title, **attrs):
        self._tpl = template_path + '.html'
        self._lbl = title
        self._hdn = None
        self._kws = attrs

    template = property(lambda self: self._tpl)

    title    = property(lambda self: self._lbl)
    heading  = property(lambda self: self._hdn or '')

    def __getitem__(self, key, default=None): return self._kws.get(key, default)

    def render(self):
        cnt = dict([(k,v) for k,v in self._kws.iteritems()])

        tpl = app.jinja_env.get_template(self.template)

        cnt['wdg']    = self
        cnt['widget'] = self

        cnt['data']   = self

        return tpl.render(cnt)

################################################################################
################################################################################

class DataProvider(object):
    def __init__(self):
        pass

    def wrap(self, raw, *args, **kwargs):
        return self.Wrapper(self, raw, *args, **kwargs)

#*******************************************************************************

class DataItem(object):
    def __init__(self, provider, raw, *args, **kwargs):
        self._prvd = provider
        self._item = raw

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prvd)
    raw_item = property(lambda self: self._item)

#*******************************************************************************

class DataBridge(object):
    def __init__(self, provider, *args, **kwargs):
        self._prvd = provider

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prvd)

################################################################################

class ApiConnector(DataBridge):
    def initialize(self, *args, **kwargs):
        self._cnx = None

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    @property
    def conn(self):
        if self._cnx is None:
            self._cnx = self.connect()

        return self._cnx

    def reset(self):
        self._cnx = None

#*******************************************************************************

class FileReader(DataBridge):
    def initialize(self, path):
        self._pth = path

    path = property(lambda self: self._pth)

    def read(self):
        return open(self.path).read()

